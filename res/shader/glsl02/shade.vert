#version 150
in vec3 inPosition; // vstup z vertex bufferu
in vec2 inTexCoord; // vstup z vertex bufferu
in vec3 inNormal; // vstup z vertex bufferu
out vec3 normal; // vystup do dalsich casti retezce
out vec4 position; // vystup do dalsich casti retezce
out vec3 viewDirection;
out vec3 lightDirection;

uniform mat4 matMV;
uniform mat4 matP;

vec3 LightPosition = vec3(10.0,10.0,10.0);

void main() {
	normal = inNormal;
    mat3 matNormal = transpose(inverse(mat3(matMV)));
    normal = matNormal*normal;
  
    vec4 lightPosition = vec4(LightPosition,1.0);
   // lightPosition = matMV * vec4(LightPosition,1.0);
   // lightPosition = vec4(0.0, 0.0, 0.0, 1.0);
   
   position = vec4(inPosition, 1.0);
   vec4 objectPosition = matMV * position;
  
   lightDirection = lightPosition.xyz - objectPosition.xyz;
   lightDirection = normalize(lightPosition.xyz);
  
   viewDirection = - objectPosition.xyz;
   
   gl_Position = matP * matMV * position;
	
   position = objectPosition;
}