#version 150
in vec4 position;
in vec3 normal;
in vec3 viewDirection;
in vec3 lightDirection;
out vec4 outColor; // vystup z fragment shaderu

vec4 Ambient = vec4(0.3,0.1,0.1,1.0);
vec4 Specular = vec4(0.01,0.5,0.01,1.0);
vec4 Diffuse = vec4(0.1,0.1,0.5,1.0);
vec4 BaseColor = vec4(1.0,1.0,1.0,1.0);

float SpecularPower = 1.0;


void main() {

   vec4 color=BaseColor;
      
   vec3 ld = normalize( lightDirection );
   vec3 nd = normalize( normal );
   vec3 vd = normalize( viewDirection );

   vec4 totalAmbient = Ambient * color;
   
   float NDotL = max(dot( nd, ld),0.0 );

   //   vec3 reflection = normalize( ( ( 2.0 * nd ) * NDotL ) - ld ); 
   //   float RDotV = max( 0.0, dot( reflection, vd ) );

   vec3 halfVector = normalize( ld + vd); 
   float NDotH = max( 0.0, dot( nd, halfVector ) );
  //  NDotH = dot( nd, halfVector);

   vec4 totalDiffuse = Diffuse * NDotL * color; 
   vec4 totalSpecular = Specular * ( pow( NDotH, SpecularPower*4.0 ) );
   //totalSpecular  = Specular * ( pow( RDotV, SpecularPower ) );

               
   outColor = totalAmbient + totalDiffuse + totalSpecular;

  //  outColor = position;
  //  outColor = vec4(ld,1.0);
  // outColor = vec4(0.5*nd + 0.5, 1.0);
  //  outColor = vec4(vec3(NDotH),1.0);
  } 

