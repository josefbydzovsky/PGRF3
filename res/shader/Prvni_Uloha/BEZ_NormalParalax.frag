#version 150

uniform sampler2D Tex_base; 
uniform sampler2D Tex_normal;  
uniform sampler2D Tex_height;  

in vec3 viewDirection;
in vec3 lightDirection;
in vec3 normal;
in float dist;
in vec2 g_TexCoord; 

void main( void )
{   
	vec4 Ambient = vec4(0.3, 0.3, 0.3, 0.3);
	vec4 Diffuse = vec4(0.6, 0.6, 0.6, 0.6);
	vec4 Specular = vec4(0.4, 0.4, 0.4, 0.3);
	float SpecularPower = 5.24640;
	  
	//vec4 BaseColor = vec4(0.1, 0.1, 0.4, 1.0);	
	vec4 BaseColor = texture2D(Tex_base,g_TexCoord.xy); 
	
	vec3 ld = normalize( lightDirection );
	vec3 nd = normalize( normal );
	vec3 vd = normalize( viewDirection );	
		
	float NDotL = max(dot( nd, ld),0.0 );

	vec3 reflection = normalize( ( ( 2.0 * nd ) * NDotL ) - ld ); 
	float RDotV = max( 0.0, dot( reflection, vd ) );

	vec3 halfVector = normalize( ld + vd); 
	float NDotH = max( 0.0, dot( nd, halfVector ) );

	vec4 totalAmbient = Ambient * BaseColor;
	vec4 totalDiffuse = Diffuse * NDotL * BaseColor; 
	vec4 totalSpecular = Specular * ( pow( NDotH, SpecularPower*4.0 ) );
	//totalSpecular  =  Specular * ( pow( RDotV, SpecularPower ) );
      
	float constantAttenuation = 0.155;
	float linearAttenuation = 0.1;
	float quadraticAttenuation = 0.0005; 
	float att=1.0/(constantAttenuation + linearAttenuation * dist + quadraticAttenuation * dist * dist);
	//att = 1.0;

	gl_FragColor = totalAmbient + att*(totalDiffuse + totalSpecular);
}