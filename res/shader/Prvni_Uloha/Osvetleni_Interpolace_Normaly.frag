#version 150
//in vec3 lightDirection;
in vec4 position;
in vec3 normal;
in vec3 lightDirection;

out vec4 outColor;

void main() {

	float NdotL = max(dot(normalize(lightDirection)	,normalize(normal))	,0.0);	
	vec3 Color=NdotL*vec3(0.3, 0.3, 0.3); // 0.3 = difuse
	
	outColor = vec4(Color.x, Color.y, Color.z+0.3 , 1.0); // vec4(Color,1.0); 
} 
