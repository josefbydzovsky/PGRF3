#version 150
in vec3 inPosition; // input from the vertex buffer
uniform mat4 mat; // variable constant for all vertices in a single draw
out vec3 Color;
void main() {
	Color = inPosition;
	Color.y *= -1;		
	
	float r = inPosition.x * 4;	
	float uhel = inPosition.y * 6.28;
	float v = 2*sin(inPosition.x * 6.28);
	
	gl_Position = mat * vec4( r*cos(uhel),	r*sin(uhel),v, 1.0);
	
} 
