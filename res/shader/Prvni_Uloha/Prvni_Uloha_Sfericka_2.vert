#version 150
in vec3 inPosition; // input from the vertex buffer
uniform mat4 mat; // variable constant for all vertices in a single draw
out vec3 Color;
void main() {
	Color = inPosition;
	Color.y *= -1;
	
	float zenit = inPosition.x* 6.28;
	float azimut = inPosition.y * 6.28;
	float r = 10*sin(4*zenit + azimut);
	
	gl_Position = mat * vec4(
	r * sin(zenit) * cos(azimut),
	r * sin(zenit) * sin(azimut),
	r * cos(zenit),
	1.0);
} 
