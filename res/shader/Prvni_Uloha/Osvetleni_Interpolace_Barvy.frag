#version 150
in vec3 Color;
out vec4 outColor;
void main() {
	outColor = vec4(Color.x, Color.y, Color.z+0.3 , 1.0); // vec4(Color,1.0); 
} 
