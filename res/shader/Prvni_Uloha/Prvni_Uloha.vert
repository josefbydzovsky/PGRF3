#version 150
in vec3 inPosition; // input from the vertex buffer
uniform mat4 mat; // variable constant for all vertices in a single draw
out vec3 Color;
void main() {
	Color = inPosition;
	Color.y *= -1;
	gl_Position = mat * vec4(inPosition, 1.0);
} 
