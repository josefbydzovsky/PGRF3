#version 150
in vec3 inPosition; 

uniform vec3 EyePosition;
uniform mat4 ModelViewProjectionMatrix; 
uniform mat4 ModelViewMatrix; 
uniform mat3 NormalMatrix;

out vec3 viewDirection;
out vec3 lightDirection;
out vec4 position;
out vec3 normal;
out float dist;

const float PI=3.1415926;

vec3 function (vec2 uv){
	float uhel1= uv.x*PI*2.0;
	float uhel2= uv.y*PI;
	vec3 position;
	position.x=5.0*cos(uhel1)*sin(uhel2);
	position.y=5.0*sin(uhel1)*sin(uhel2);
	position.z=5.0*cos(uhel2);
	return position;
}  
   
vec3 normalDiff (vec2 uv){
   float delta = 0.01;
   vec3 dzdu = (function(uv+vec2(delta,0))-function(uv-vec2(delta,0)))/2.0/delta;
   vec3 dzdv = (function(uv+vec2(0,delta))-function(uv-vec2(0,delta)))/2.0/delta;
   return cross(dzdu,dzdv);
} 

void main( void )
{
   vec3 LightPosition= vec3(15.0,15.0,0.0);
   
   vec4 position = vec4(1.0);
   position.xyz = function(inPosition.xy);
   
   normal = normalDiff(inPosition.xy);
   normal = NormalMatrix*normal;   
   
   vec4 lightPosition = vec4(LightPosition,1.0);
   lightPosition = ModelViewMatrix * lightPosition;
   
   vec4 objectPosition = ModelViewMatrix * position;
  
   lightDirection = lightPosition.xyz - objectPosition.xyz;
   dist=length(lightDirection);
  
   viewDirection = EyePosition.xyz - objectPosition.xyz;
   //viewDirection = - objectPosition.xyz;
   
   gl_Position= ModelViewProjectionMatrix*position;
}






