#version 150
in vec3 inPosition; 

uniform vec3 EyePosition;
uniform mat4 ModelViewProjectionMatrix; 
uniform mat4 ModelViewMatrix; 
uniform mat3 NormalMatrix;

out vec3 viewDirection;
out vec3 lightDirection;
out vec4 position;
out vec3 normal;
out float dist;
out vec2 g_TexCoord;

void main( void )
{
   vec3 LightPosition= vec3(0.0,0.0,1.0);
   g_TexCoord = inPosition.xy;
   g_TexCoord.y *= -1;
   
   vec4 position = vec4(1.0);
   position.xyz = inPosition.xyz;
   position.x *= 5.0;
   position.y *= 5.0;
   
   normal = vec3(0.0, 0.0, 1.0);
   normal = NormalMatrix*normal;   
   
   vec4 lightPosition = vec4(LightPosition,1.0);
   lightPosition = ModelViewMatrix * lightPosition;
   
   vec4 objectPosition = ModelViewMatrix * position;
  
   lightDirection = lightPosition.xyz - objectPosition.xyz;
   dist=length(lightDirection);
  
   viewDirection = EyePosition.xyz - objectPosition.xyz;
   //viewDirection = - objectPosition.xyz;
   
   gl_Position= ModelViewProjectionMatrix*position;
}






