#version 150
in vec3 Color;
out vec4 outColor; // output from the fragment shader
void main() {
	outColor = vec4(Color.x, Color.y, 1-(Color.x+Color.y) , 1.0); 
} 
