#version 150
in float tPosition; 
out vec3 vertColor; // output from this shader to the next pipleline stage
uniform mat4 mat; // variable constant for all vertices in a single draw

vec3 curve(float t){
	return vec3(
		cos(40 * 3.14 * t),
		sin(40 * 3.14 * t),
		2 + t	
	);
}

void main() {


	gl_Position = mat * vec4(curve(tPosition), 1.0);
	
	
	vertColor = vec3(0, 1 - tPosition, tPosition);
} 
