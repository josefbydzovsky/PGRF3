package glsl03_texture_1;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;

import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseEvent;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import oglutils.OGLBuffers;
import oglutils.OGLTextRenderer;
import oglutils.OGLUtils;
import oglutils.ShaderUtils;
import oglutils.ToFloatArray;
import transforms.Camera;
import transforms.Mat4;
import transforms.Mat4PerspRH;
import transforms.Vec3D;

/**
 * GLSL sample:<br/>
 * Load texture and apply it to cube faces<br/>
 * Requires JOGL 2.3.0 or newer
 * 
 * @author PGRF FIM UHK
 * @version 2.0
 * @since   2015-09-05 
 */

public class Renderer implements GLEventListener, MouseListener,
		MouseMotionListener, KeyListener {

	int width, height, ox, oy;

	OGLBuffers buffers;
	OGLTextRenderer textRenderer = new OGLTextRenderer();

	int shaderProgram, locMat;
	
	Texture texture;

	Camera cam = new Camera();
	Mat4 proj;

	public void init(GLAutoDrawable glDrawable) {
		GL2 gl = glDrawable.getGL().getGL2();

		OGLUtils.printOGLparameters(gl);
		OGLUtils.shaderCheck(gl);
		
		// shader files are in /res/shader/ directory
		// res directory must be set as a source directory of the project
		// e.g. in Eclipse via main menu Project/Properties/Java Build Path/Source
		shaderProgram = ShaderUtils.loadProgram(gl, "/shader/glsl03/texture");
		createBuffers(gl);

		locMat = gl.glGetUniformLocation(shaderProgram, "mat");
		
		// load texture using JOGL objects
		// texture files are in /res/textures/
		try {
			System.out.print("Loading texture...");
			texture = TextureIO.newTexture(new File("res/textures/mosaic.jpg"), true);
			System.out.println("ok");
		} catch (IOException e) {
			System.out.println("failed");
			System.out.println(e);
		}
		
		cam = cam.withPosition(new Vec3D(5, 5, 2.5))
				.withAzimuth(Math.PI * 1.25)
				.withZenith(Math.PI * -0.125);

		gl.glEnable(GL2.GL_DEPTH_TEST);
	}

	void createBuffers(GL2 gl) {
		float[] cube = {
				// bottom (z-) face
				0, 0, 0,	0, 0, -1,	0, 0, 
				1, 0, 0,	0, 0, -1,	1, 0, 
				1, 1, 0,	0, 0, -1,	1, 1, 
				0, 1, 0,	0, 0, -1,	0, 1,
				// top (z+) face
				0, 0, 1,	0, 0, 1,	0, 0,
				1, 0, 1,	0, 0, 1,	1, 0,
				1, 1, 1,	0, 0, 1,	1, 1,
				0, 1, 1,	0, 0, 1,	0, 1,
				// x+ face
				1, 0, 0,	1, 0, 0,	0, 0,
				1, 1, 0,	1, 0, 0,	1, 0,
				1, 1, 1,	1, 0, 0,	1, 1,
				1, 0, 1,	1, 0, 0,	0, 1,
				// x- face
				0, 0, 0,	-1, 0, 0,	0, 0,
				0, 1, 0,	-1, 0, 0,	1, 0,
				0, 1, 1,	-1, 0, 0,	1, 1,
				0, 0, 1,	-1, 0, 0,	0, 1,
				// y+ face
				0, 1, 0,	0, 1, 0,	0, 0,
				1, 1, 0,	0, 1, 0,	1, 0,
				1, 1, 1,	0, 1, 0,	1, 1,
				0, 1, 1,	0, 1, 0,	0, 1,
				// y- face
				0, 0, 0,	0, -1, 0,	0, 0,
				1, 0, 0,	0, -1, 0,	1, 0,
				1, 0, 1,	0, -1, 0,	1, 1,
				0, 0, 1,	0, -1, 0, 	0, 1
		};

		OGLBuffers.Attrib[] attributes = {
				new OGLBuffers.Attrib("inPosition", 3),
				new OGLBuffers.Attrib("inNormal", 3),
				new OGLBuffers.Attrib("inTextureCoordinates", 2)
		};

		buffers = new OGLBuffers(gl, cube, attributes, null);
	}
	
	void bindTexture(GL2 gl) {
		// bind texture to a shader uniform variable via a texture slot 0
		// first bind texture to slot 0
		gl.glActiveTexture(GL2.GL_TEXTURE0); // slot 0
		texture.bind(gl);
		// then bind shader variable to slot 0
		int locTexture = gl.glGetUniformLocation(shaderProgram, "texture");
		gl.glUniform1i(locTexture, 0); // slot 0
		// these steps can be performed independently (e.g. bind another texture
		// to slot 0 without rebinding shader variable)
	}

	
	public void display(GLAutoDrawable glDrawable) {
		GL2 gl = glDrawable.getGL().getGL2();
		
		gl.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
		
		gl.glUseProgram(shaderProgram); 
		gl.glUniformMatrix4fv(locMat, 1, false,
				ToFloatArray.convert(cam.getViewMatrix().mul(proj)), 0);
		
		bindTexture(gl);
		
		buffers.draw(GL2.GL_QUADS, shaderProgram);
		
		String text = new String(this.getClass().getName() + ": [LMB] camera, WSAD");
		
		textRenderer.drawStr2D(glDrawable, 3, height-20, text);
		textRenderer.drawStr2D(glDrawable, width-90, 3, " (c) PGRF UHK");
	}

	public void reshape(GLAutoDrawable drawable, int x, int y, int width,
			int height) {
		this.width = width;
		this.height = height;
		proj = new Mat4PerspRH(Math.PI / 4, height / (double) width, 0.01, 1000.0);
	}

	public void displayChanged(GLAutoDrawable drawable, boolean modeChanged,
			boolean deviceChanged) {
	}

	public void mouseClicked(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
		ox = e.getX();
		oy = e.getY();
	}

	public void mouseReleased(MouseEvent e) {
	}

	public void mouseDragged(MouseEvent e) {
		cam = cam.addAzimuth((double) Math.PI * (ox - e.getX()) / width)
				.addZenith((double) Math.PI * (e.getY() - oy) / width);
		ox = e.getX();
		oy = e.getY();
	}

	public void mouseMoved(MouseEvent e) {
	}

	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_W:
			cam = cam.forward(1);
			break;
		case KeyEvent.VK_D:
			cam = cam.right(1);
			break;
		case KeyEvent.VK_S:
			cam = cam.backward(1);
			break;
		case KeyEvent.VK_A:
			cam = cam.left(1);
			break;
		case KeyEvent.VK_CONTROL:
			cam = cam.down(1);
			break;
		case KeyEvent.VK_SHIFT:
			cam = cam.up(1);
			break;
		case KeyEvent.VK_SPACE:
			cam = cam.withFirstPerson(!cam.getFirstPerson());
			break;
		case KeyEvent.VK_R:
			cam = cam.mulRadius(0.9f);
			break;
		case KeyEvent.VK_F:
			cam = cam.mulRadius(1.1f);
			break;
		}
	}

	public void keyReleased(KeyEvent e) {
	}

	public void keyTyped(KeyEvent e) {
	}

	public void dispose(GLAutoDrawable glDrawable) {
		GL2 gl = glDrawable.getGL().getGL2();
		gl.glDeleteProgram(shaderProgram);
	}
}