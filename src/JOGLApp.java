import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.FPSAnimator;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


public class JOGLApp {
	private static final int FPS = 60; // animator's target frames per second

	private GLCanvas canvas = null;

	private Frame testFrame;
	
	static String[] names = { "glsl01_start_0", "glsl01_start_1", "glsl01_start_2", "glsl01_start_3", "glsl01_start_4", "glsl01_start_5",
			"glsl02_cube", "glsl02_obj", "glsl03_texture_1", "glsl03_texture_2",
			"glsl03_textureCube", "glsl03_textureVolume",
			"glsl04_renderTarget_1", "glsl04_renderTarget_2", "glsl04_renderTarget_3_gpgpu",
			"glsl04_renderTarget_4_save", "glsl04_renderTarget_5_draw", "glsl04_renderTarget_6_BufferedImage",
			"glsl04_renderTarget_7_moreOutputs", "glsl04_renderTarget_8_postProcessing", "glsl05_geometry",
			"glsl05_tessellation" };

	public void start() {
		try {
			testFrame = new Frame("TestFrame");
			testFrame.setSize(512, 384);

			makeGUI(testFrame);

			setApp(testFrame, 1);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void makeGUI(Frame testFrame) {
		ActionListener actionListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				setApp(testFrame, Integer.valueOf(ae.getActionCommand().substring(0,ae.getActionCommand().lastIndexOf('-')-1).trim()));
			}
		};

		MenuBar menuBar = new MenuBar();
		Menu menu = new Menu("Menu");
		MenuItem m;
		for (int i = 0; i < names.length; i++) {
			m = new MenuItem(new Integer(i+1).toString()+" - "+names[i]);
			m.addActionListener(actionListener);
			menu.add(m);
		}
		
		menuBar.add(menu);
		testFrame.setMenuBar(menuBar);
	}

	private void setApp(Frame testFrame, int type) {
		if (canvas != null) 
			testFrame.remove(canvas);

		// setup OpenGL Version 2
		GLProfile profile = GLProfile.get(GLProfile.GL2);
		GLCapabilities capabilities = new GLCapabilities(profile);
		capabilities.setRedBits(8);
		capabilities.setBlueBits(8);
		capabilities.setGreenBits(8);
		capabilities.setAlphaBits(8);
		capabilities.setDepthBits(24);

		canvas = new GLCanvas(capabilities);
		canvas.setSize(512, 384);
		testFrame.add(canvas);
		
		Object ren=null;
		Class<?> renClass;
		try {
			renClass = Class.forName(names[type-1]+".Renderer");
			ren = renClass.newInstance();
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e1) {
			e1.printStackTrace();
		};

		canvas.addGLEventListener((GLEventListener)ren);
		canvas.addKeyListener((KeyListener)ren);
		canvas.addMouseListener((MouseListener)ren);
		canvas.addMouseMotionListener((MouseMotionListener)ren);

		final FPSAnimator animator = new FPSAnimator(canvas, FPS, true);

		testFrame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				new Thread() {
					@Override
					public void run() {
						if (animator.isStarted())
							animator.stop();
						System.exit(0);
					}
				}.start();
			}
		});
		//testFrame.setTitle(getClass().getName());

		testFrame.pack();
		testFrame.setVisible(true);
		animator.start(); // start the animation loop

	}

	public static void main(String[] args) {
		new JOGLApp().start();
	}

}