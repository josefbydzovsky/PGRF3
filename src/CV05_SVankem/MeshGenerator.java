package CV05_SVankem;

import com.jogamp.opengl.GL2;

import oglutils.OGLBuffers;

public class MeshGenerator {
	public static OGLBuffers createLineStrip(GL2 gl , final int n, final String atributeNameKteryNepouzivas){
		int n1 = n;
		if(n1 < 2) n1=2;
		
		OGLBuffers.Attrib[] attributes = { new OGLBuffers.Attrib(atributeNameKteryNepouzivas, 1)};
		
		float posun = 1.0f/(n-1);
		float[] mesh = new float[n];		
		float i = 0;
		int o = 0;
		for(; o < (n-1); o++){
			mesh[o] = i;
			i+=posun;
		}
		mesh[o] = 1f;
		
		int[] index = new int[n];
		for(int t = 0; t<n;t++){
			index[t]=t;
		}		
		OGLBuffers b = new OGLBuffers(gl, mesh, attributes, index);
		return b;
	}

}
