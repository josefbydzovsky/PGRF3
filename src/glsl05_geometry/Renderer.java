package glsl05_geometry;

import com.jogamp.opengl.DebugGL2;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;

import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseEvent;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;

import oglutils.OGLBuffers;
import oglutils.OGLTextRenderer;
import oglutils.OGLUtils;
import oglutils.ShaderUtils;

/**
 * Ukazka pro praci s shadery v GLSL
 * pouziti geometry shaderu
 * 
 * @author PGRF FIM UHK
 * @version 2.0
 * @since   2015-09-06 
 */

public class Renderer implements GLEventListener, MouseListener,
		MouseMotionListener, KeyListener {

	int width, height;

	OGLBuffers buffers;
	OGLTextRenderer textRenderer = new OGLTextRenderer();
	
	int shaderProgram, locTime;

	float time = 0;

	
	public void init(GLAutoDrawable glDrawable) {
		GL2 gl = glDrawable.getGL().getGL2();

		glDrawable.setGL(new DebugGL2(gl));
		gl = glDrawable.getGL().getGL2();
		
		OGLUtils.printOGLparameters(gl);
		OGLUtils.shaderCheck(gl);
		
		if (OGLUtils.getVersion(gl) < 300){
			System.err.println("Geometry shader is not supported"); 
			System.exit(0);
		}
		
		String extensions = gl.glGetString(GL2.GL_EXTENSIONS);
			if (extensions.indexOf("GL_ARB_enhanced_layouts") == -1)
				shaderProgram = ShaderUtils.loadProgram(gl, "/shader/glsl05/geometry_OlderSM");
			else
				shaderProgram = ShaderUtils.loadProgram(gl, "/shader/glsl05/geometry");
		createBuffers(gl);
		
		locTime = gl.glGetUniformLocation(shaderProgram, "time");
	}
	
	void createBuffers(GL2 gl) {
		int[] indexBufferData = { 0, 1, 2, 3, 4};
		
		float[] vertexBufferDataPos = {
			-0.5f, 0.0f, 
			0.0f, 0.5F,
			0.0f, -0.5f, 
			0.5f, 0.0f, 
			0.8f, 0.5f 
				};
		float[] vertexBufferDataCol = {
			0, 1, 0, 
			1, 0, 0,
			1, 1, 0,
			0, 0, 1, 
			0, 1, 1 
		};
		OGLBuffers.Attrib[] attributesPos = {
				new OGLBuffers.Attrib("inPosition", 2),
		};
		OGLBuffers.Attrib[] attributesCol = {
				new OGLBuffers.Attrib("inColor", 3)
		};
		buffers = new OGLBuffers(gl, vertexBufferDataPos, attributesPos,
				indexBufferData);
		buffers.addVertexBuffer(vertexBufferDataCol, attributesCol);

	}



	public void display(GLAutoDrawable glDrawable) {
		GL2 gl = glDrawable.getGL().getGL2();
		
		gl.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);

		time += 0.01;
		// nastaveni aktualniho shaderu, v teto ukazce nadbytecne
		gl.glUseProgram(shaderProgram); 
		gl.glUniform1f(locTime, time); // musi byt nastaven spravy shader
		
		// vykresleni
		buffers.draw(GL3.GL_LINE_STRIP_ADJACENCY, shaderProgram);
		
		String text = new String(this.getClass().getName());
		
		textRenderer.drawStr2D(glDrawable, 3, height-20, text);
		textRenderer.drawStr2D(glDrawable, width-90, 3, " (c) PGRF UHK");
	}

	public void reshape(GLAutoDrawable drawable, int x, int y, int width,
			int height) {
		this.width = width;
		this.height = height;
	}

	public void displayChanged(GLAutoDrawable drawable, boolean modeChanged,
			boolean deviceChanged) {
	}

	public void mouseClicked(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
	}

	public void mouseDragged(MouseEvent e) {
	}

	public void mouseMoved(MouseEvent e) {
	}

	public void keyPressed(KeyEvent e) {
	}

	public void keyReleased(KeyEvent e) {
	}

	public void keyTyped(KeyEvent e) {
	}

	public void dispose(GLAutoDrawable glDrawable) {
		GL2 gl = glDrawable.getGL().getGL2();
		gl.glDeleteProgram(shaderProgram);
	}

}