package Prvni_Uloha;

import com.jogamp.opengl.GL2;

import oglutils.OGLBuffers;

public class GridGenerator {
	public static OGLBuffers createGrid(GL2 gl , final int clenitost){
		//pojistka
		int clenitost2 = clenitost;
		if(clenitost2 < 2){
			clenitost2=2;
		}
		
		//atributy
		OGLBuffers.Attrib[] attributes = { new OGLBuffers.Attrib("inPosition", 3)};
		
		/*
		jednorozm�rn� vertexbuffer
		float i = 0;
		int o = 0;
		for(; o < (clenitost-1); o++){
			grid[o] = i;
			i+=posun;
		}
		grid[o] = 1f;
		*/
		
		
		// vertex buffer
		float[] grid = generateGrid(clenitost);
		
		/*
		//kontroln� v�pis
		for(int i = 0; i < grid.length; i++){
			System.out.println(grid[i]);
		}
		*/
		// <-- vertex buffer	
		
		
		// index buffer		
		int[] indexBuffer = generateIndexBuffer(clenitost);
		
		
		/*
		//kontroln� v�pis
		for(int w = 0; w<indexBuffer.length;w++){
			System.out.println(indexBuffer[w]);
		}
		*/
		// <-- index buffer
		
		OGLBuffers b = new OGLBuffers(gl, grid, attributes, indexBuffer);
		return b;
	}
	
	private static float[] generateGrid(int clenitost) {
		int i = 0;
		float posun = 1.0f/(clenitost-1);
		float[] grid = new float[clenitost*clenitost*3];
		//��dky
		int r = 0;
		for(; r < (clenitost-1); r++){
			//sloupce
			int s = 0;
			for(; s < (clenitost-1);s++){
				//x
				grid[i] = s*posun;
				i++;
				//y
				grid[i] = -r*posun;
				i++;
				//z
				grid[i] = 0;
				i++;			
			}
			//posledn� sloupce v�dycky s jedni�kou v x
			// x
			grid[i] = 1f;
			i++;
			// y
			grid[i] = -r * posun;
			i++;
			// z
			grid[i] = 0;
			i++;
		}
		//posledn� ��dek s jedni�kou v y
		for(int wer = 0; wer < (clenitost-1);wer++){
			//x
			grid[i] = wer*posun;
			i++;
			//y
			grid[i] = -1f;
			i++;
			//z
			grid[i] = 0;
			i++;	
		}
		//posledn� bod(prav� doln� roh)
		//x
		grid[i] = 1f;
		i++;
		//y
		grid[i] = -1f;
		i++;
		//z
		grid[i] = 0;
		i++;	
		
		return grid;
	}

	private static int[] generateIndexBuffer(int clenitost) {
		int[] indexBuffer = new int[2*clenitost*clenitost - clenitost - 2];
		int i = 0;
		//��dky
		for(int r = 0; r < (clenitost-1); r++){
			// sloupce
			//zleva doprava
			if(r % 2 == 0){
				for(int s = 0; s < clenitost; s++){
					indexBuffer[i] = r*clenitost+s;
					i++;
					indexBuffer[i] = (r+1)*clenitost+s;
					i++;
					if(s == (clenitost-1) && r != (clenitost-2)){
						indexBuffer[i] = (r+1)*clenitost+s;
						i++;						
					}
				}				
			}else{ // zpravad doleva
				for(int s = clenitost-1; s >= 0; s--){
					indexBuffer[i] = r*clenitost+s;
					i++;
					indexBuffer[i] = (r+1)*clenitost+s;
					i++;
					if(s == 0 && r != (clenitost-2)){
						indexBuffer[i] = (r+1)*clenitost+s;
						i++;						
					}
				}	
			}
		}
		return indexBuffer;
	}
	/*
	public static void main(String[] args) {
		createGrid(null, 6);
	}
	*/
}
