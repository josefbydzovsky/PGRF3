package Prvni_Uloha;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;

import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.LinkedList;
import java.awt.event.MouseEvent;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;

import oglutils.OGLBuffers;
import oglutils.OGLTextRenderer;
import oglutils.OGLUtils;
import oglutils.ShaderUtils;
import oglutils.ToFloatArray;
import transforms.Camera;
import transforms.Mat3;
import transforms.Mat4;
import transforms.Mat4PerspRH;
import transforms.Vec3D;

/**
 * GLSL sample:<br/>
 * Draw 3D geometry, use camera and projection transformations<br/>
 * Requires JOGL 2.3.0 or newer
 * 
 * @author PGRF FIM UHK
 * @version 2.0
 * @since 2015-09-05
 */

public class Renderer_Osvetleni_Interpolace_Barvy implements GLEventListener, MouseListener,
		MouseMotionListener, KeyListener {
	//params starého vykreslování
	int velikostGridu = 250;
	float sirkaTrojuhelniku = 0.3f;
	
	// parametr nového vykreslování
	int clenitost = 5;

	int width, height, ox, oy;
	
	long oldmils;
	double fps = 0;

	//starý vykreslování
	LinkedList<OGLBuffers> buffers = new LinkedList<>();
	
	OGLBuffers buffer;
	
	OGLTextRenderer textRenderer = new OGLTextRenderer();

	int shaderProgram, locModelViewMatrix,locModelViewProjectionMatrix,locNormalMatrix;

	Camera cam = new Camera();
	Mat4 proj; // created in reshape()
	float posun = velikostGridu*sirkaTrojuhelniku/2;

	public void init(GLAutoDrawable glDrawable) {
		GL2 gl = glDrawable.getGL().getGL2();
		
		OGLUtils.printOGLparameters(gl);
		OGLUtils.shaderCheck(gl);

		gl.glPolygonMode(gl.GL_BACK , gl.GL_LINE);
		gl.glPolygonMode(gl.GL_FRONT , gl.GL_FILL);
		
		// shader files are in /res/shader/ directory
		// res directory must be set as a source directory of the project
		// e.g. in Eclipse via main menu Project/Properties/Java Build Path/Source
		shaderProgram = ShaderUtils.loadProgram(gl, "/shader/Prvni_Uloha/Osvetleni_Interpolace_Barvy");
		
		//starý vykreslování
		//createBuffers(gl);
		
		buffer = GridGenerator.createGrid(gl, clenitost);

		locModelViewProjectionMatrix = gl.glGetUniformLocation(shaderProgram, "ModelViewProjectionMatrix");
		locModelViewMatrix = gl.glGetUniformLocation(shaderProgram, "ModelViewMatrix");
		locNormalMatrix = gl.glGetUniformLocation(shaderProgram, "NormalMatrix");

		cam = cam.withPosition(new Vec3D(10.2, 2.0, 2.0))
				.withAzimuth(9.51)
				.withZenith(-0.07);
		
		gl.glEnable(GL2.GL_DEPTH_TEST);
	}

	
	public void display(GLAutoDrawable glDrawable) {
		GL2 gl = glDrawable.getGL().getGL2();
		
		gl.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
		
		gl.glUseProgram(shaderProgram); 
		gl.glUniformMatrix4fv(locModelViewProjectionMatrix, 1, false, ToFloatArray.convert(cam.getViewMatrix().mul(proj)), 0);
		gl.glUniformMatrix4fv(locModelViewMatrix, 1, false, ToFloatArray.convert(cam.getViewMatrix()), 0);

		double[][] fl = new double[3][3];
		for(int r= 0; r<3;r++){
			for(int s = 0; s<3;s++){
				fl[r][s] = cam.getViewMatrix().get(r, s);
			}
		}
		Mat3 m = new Mat3(fl);
		m = m.inverse().get().transpose();
		gl.glUniformMatrix3fv(locNormalMatrix, 1, false, ToFloatArray.convert(m), 0);
		
		/*
		starý vykreslování
		for(int i = 0; i < velikostGridu; i++){
			buffers.get(i).draw(GL2.GL_TRIANGLE_STRIP, shaderProgram);		
		}
		*/
		buffer.draw(GL2.GL_TRIANGLE_STRIP, shaderProgram);

		//fps
		long mils = System.currentTimeMillis();
		if ((mils - oldmils)>0){
			fps = 1000 / (double)(mils - oldmils + 1);
			oldmils=mils;
		}
		String text = new String("FPS = " + String.format("%3.1f", fps));		
		textRenderer.drawStr2D(glDrawable, 3, height-20, text);
	}

	public void reshape(GLAutoDrawable drawable, int x, int y, int width,
			int height) {
		this.width = width;
		this.height = height;
		proj = new Mat4PerspRH(Math.PI / 4, height / (double) width, 0.01, 1000.0);
	}

	public void displayChanged(GLAutoDrawable drawable, boolean modeChanged,
			boolean deviceChanged) {
	}

	public void mouseClicked(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
		ox = e.getX();
		oy = e.getY();
	}

	public void mouseReleased(MouseEvent e) {
	}

	public void mouseDragged(MouseEvent e) {
		cam = cam.addAzimuth((double) Math.PI * (ox - e.getX()) / width)
				.addZenith((double) Math.PI * (oy - e.getY()) / width);
		ox = e.getX();
		oy = e.getY();
	}

	public void mouseMoved(MouseEvent e) {
	}

	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_W:
			cam = cam.forward(0.3);
			break;
		case KeyEvent.VK_D:
			cam = cam.right(0.3);
			break;
		case KeyEvent.VK_S:
			cam = cam.backward(0.3);
			break;
		case KeyEvent.VK_A:
			cam = cam.left(0.3);
			break;
		case KeyEvent.VK_CONTROL:
			cam = cam.down(0.3);
			break;
		case KeyEvent.VK_SHIFT:
			cam = cam.up(0.3);
			break;
		case KeyEvent.VK_SPACE:
			cam = cam.withFirstPerson(!cam.getFirstPerson());
			break;
		case KeyEvent.VK_R:
			cam = cam.mulRadius(0.9f);
			break;
		case KeyEvent.VK_F:
			cam = cam.mulRadius(1.1f);
			break;
		}
	}

	public void keyReleased(KeyEvent e) {
	}

	public void keyTyped(KeyEvent e) {
	}

	public void dispose(GLAutoDrawable glDrawable) {
		GL2 gl = glDrawable.getGL().getGL2();
		gl.glDeleteProgram(shaderProgram);
	}

	void createBuffers(GL2 gl) {
		
		OGLBuffers.Attrib[] attributes = { new OGLBuffers.Attrib("inPosition", 3)};
		//buffers.add( new OGLBuffers(gl, grid, attributes, indexBuffer));	

		// "řádky"
		for(int i = 0; i < velikostGridu; i++){	
			float[] grid = new float[(velikostGridu*2+2)*3];
			//sloupce
			for(int j = 0; j <= velikostGridu; j++){
				grid[j*2*3+0] = j*sirkaTrojuhelniku; 
				grid[j*2*3+1] = i*sirkaTrojuhelniku; 
				grid[j*2*3+2] = 0; 
				
				grid[j*2*3+3] = j*sirkaTrojuhelniku; 
				grid[j*2*3+4] = i*sirkaTrojuhelniku+sirkaTrojuhelniku; ; 
				grid[j*2*3+5] = 0; 
			}		
			int[] indexBuffer = new int[grid.length/3];
			for(int p = 0; p < indexBuffer.length; p++){
				indexBuffer[p] = p;
			}
			buffers.add(new OGLBuffers(gl, grid, attributes, indexBuffer));			
		}
	}

}